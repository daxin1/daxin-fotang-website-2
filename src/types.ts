// berisi interface tipe data

export interface NavItem {
    icon: string;
    title: string;
    url: string;
}