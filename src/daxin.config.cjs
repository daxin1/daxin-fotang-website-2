export default {
    siteTitle: "DaXin Fotang Website",
    siteDescription: "Situs Informasi Fotang DaXin",
    favicon: "/favicon.ico",
    // siteImagePath: "/images/sarissa.png",
    footer: "© 2018 DaXin Fotang",
    dateFormat: "dd.MM.yyyy HH:mm",
    pageSize: 5,
    categories: [
      {
        name: "theme",
        color: "btn-warning",
        image: "/images/theme.jpg",
        order: 1,
      },
    ],
    categorySettings: {
      order: "name", // name | count
      layout: "card", //button | card
      image: "",
      color: "btn-primary",
      countVisibility: true,
    },
    searchOptions: {
      includeScore: true,
      includeMatches: true,
      keys: [
        { name: "title", weight: 3 },
        { name: "description", weight: 2 },
      ],
    },
    i18n: {
      search: {
        placeholder: "Search post title and description...",
      },
      archive: {
        select: "Select Year",
      },
      page: "Page",
      resultFound: " result(s) found",
    },
  };